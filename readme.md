> IP通过学校的路由器动态分配，服务器设置静态IP地址会导致网络无法使用。服务器IP无法使用时，或需要新的IP地址，请联系管理员。

>  当前为测试版第一版，可能存在许多未知的bug，在使用过程中，欢迎反馈或者帮助修复一些已知的问题。

[toc]


# 服务器使用指南
> 第一章是如何登录服务器的基本内容，建议全部仔细阅读；第二、三章分别为matlab、python的使用教程，建议按需阅读；服务器目前采用的是ubuntu20.04，建议同学们课余时间补充学习linux基本命令。

## 服务器配置

1. 团队目前有两台服务器分别为GPU和CPU的。
	- 跑matlab的推荐使用CPU的服务器。
	- 跑深度的推荐使用GPU的服务器。
2. 提供5个固定用户分别为：任意选择一个，然后每次固定使用这个即可。

账号|密码
---|---
stu01|501
stu02|501
stu03|501
stu04|501
stu05|501
3. 可能存在多个人共用一个服务器，建议新建一个自己名字命名的文件夹，将所有内容放入自己的文件夹中，并且做好重要数据备份，避免数据丢失。

>后文中，涉及服务器地址均为：~~`10.24.3.41`~~

> 指令中出现`<str>`的字符串时，操作过程中需要根据情况进行替换，不能直接复制粘贴
例如出现`<username>`时，需要将命令中的`<username>`替换成自己的用户名。


# 1. MobaXterm使用SSH、SFTP
> `MobaXterm`集成了连接终端要用到的很多协议，本指南多数操作均在`MabaXterm`上进行，该软件还有很多高级的功能，可以自行去了解。QQ群提供了破解安装包。

## 1.1 SSH登录
- 具体操作步骤如图所示。
<div align="center">
    	<img src="./pic/2021-09-29-21-42-51.png"  style="zoom:100%;"">  
</div>

### 1.1.1 首次配置
- 建议新建一个个人文件，，文件上传操作都在个人文件夹内进行

## 1.2 SFTP登录
- 具体步骤基本与SSH登录相同，唯一区别是第二步选择`SFTP`协议。SFTP等于是连上服务器的文件系统。打开特定的目录，通过**拖拽**实现文件上下传。

# 2. 使用MATLAB

## 2.1 开启服务

- 服务只需要开启一次，断电重启才需要重新开启一个服务，否则可以一直使用，不要多次启动多个服务。

```bash
vncserver -localhost no -geometry 1920x1080
```
<div align="center">
    	<img src="./pic/2022-11-25-16-27-40.png"  style="zoom:100%;"">  
</div>

- 使用`MobaXTerm`连接vnc服务
<div align="center">
    	<img src="./pic/2022-11-25-16-37-46.png"  style="zoom:60%;"">  
</div>

- 以下窗口需要输入`vnc`服务的密码，即`123456`
<div align="center">
    	<img src="./pic/2022-11-25-16-41-00.png"  style="zoom:60%;"">  
</div>

- 具体如何使用matlab，见下图。其他操作，跟ubuntu系统完全一致，百度即可。
<div align="center">
    	<img src="./pic/2022-11-25-17-02-41.png"  style="zoom:70%;"">  
</div>


## 2.2 提升MATLAB使用体验

### 2.2.1 设置快捷键
- linux下matlab的快捷键很奇怪，下图提供更改为windows快捷键的方法。

  > 因为电脑的中文输入法跟matlab冲突，需要切换到英文模式使用方向键，否则可能摁一下方向键出现 一堆2\4\6\8的数字

<div align="center">
    	<img src="./pic/2021-09-29-22-56-43.png"  style="zoom:100%;"">  
</div>

### 2.2.2 调整标题栏

- 高分辨率的屏幕下，`matlab`的标题栏等特别小，在`matlab`的命令栏敲下面两行代码，可以放大比例，命令为`1.25`倍，可以按自己需求调整

```matlab
s = settings;s.matlab.desktop.DisplayScaleFactor
s.matlab.desktop.DisplayScaleFactor.PersonalValue = 1.25
```

# 3. 使用PYTHON

> 下面教程只提供使用思路，其实更建议自己去下载一下anaconda放到自己的目录，然后配合教程使用。


## 3.1 首次配置

- 使用下面的之类，初始化conda
```bash
/opt/anaconda3/bin/conda init
```
- 给conda添加镜像源
```bash
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/r/
```
- 创建一个属于自己的py环境，按需求设置环境名和python版本
```bash
conda create -n <pyEnvName> python=3.7
```

<div align="center">
    	<img src="./pic/2021-09-29-23-20-24.png" style="zoom:100%;"">  
</div>



- 登录时加载的默认环境，使用SFTP，打开个人目录(`/home/<userName>`)下的`.bashrc`文件，在最后添加下面的指令，关闭并保存文件。如果想用别的环境作为启动的默认环境，可以在这里修改对应的环境名称。
```bash
conda activate <pyEnvName>
```
<div align="center">
    	<img src="./pic/2021-10-05-16-10-24.png" style="zoom:100%;"">  
</div>

- 在终端执行`source ~/.bashrc`，可以更新当前python环境。

- 给当前环境pip添加镜像源
```bash
pip config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple
```

> 当然，如果需要用到不同的python版本，torch版本之类，可以用上述方法创建多个不同的python环境，通过`conda activate`切换，具体可以百度。

---
> 如果需要使用`cuda`，继续往下看，否则，python 的首次配置已经结束。

- 建议通过`SFTP`协议打开文件`/home/<username>/.bashrc` 

<div align="center">
    	<img src="./pic/2021-09-29-23-40-03.png"  style="zoom:100%;"">  
</div>

- 将以下内容粘贴到文件最后面，关闭并保存`.bashrc`文件。
```bash
# CUDA ENV
CUDA_HOME="/usr/local/cuda"
export PATH="$CUDA_HOME/bin:$PATH"
export LD_LIBRARY_PATH="$CUDA_HOME/lib64:$LD_LIBRARY_PATH"
export LIBRARY_PATH="$CUDA_HOME/lib64:$LIBRARY_PATH"
```

- 在终端执行一次，`source ~/.bashrc`

- 使用`nvcc -V`，查看cuda版本号，注意是大写的`V`，如果正常，会有如下显示。
> `RTX 30系显卡好像只能装cuda 11.0+，低版本的会有问题，我(科艺)尝试多版本共存没有成功，如果有人发现了30系显卡cuda多版本共存的方案的话，可以分享给我，以兼容低版本的torch架构`
<div align="center">
    	<img src="./pic/2021-09-29-23-49-37.png"  style="zoom:100%;"">  
</div>


- 安装`torch`之后对`cuda`和`cudnn`进行测试，以下全部通过，则完成torch配置

```python
import torch
# 若正常则静默

from torch.backend import cudnn
# 若正常则静默

test = torch.tensor(1.)
# 若正常则静默

test.cuda()
# 若正常则返回 tensor(1., device='cuda:0')


cudnn.is_available()
# 若正常则返回 True

cudnn.is_acceptable(test.cuda())
# 若正常则返回 True
```

## 3.2 提高PYTHON使用体验

### 3.2.1 设置软链接
> 请务必设置！！！ 
- 
```bash
ll ~/bin
```
- 如果提示，不存在这样的目录，则新建一个该目录，否则跳过该步骤
```bash
mkdir ~/bin
```
- 查看python环境路径，注意一定不要使用base环境！！
```
conda info --env
```
<div align="center">
    	<img src="./pic/2021-10-05-15-26-52.png"  style="zoom:100%;"">  
</div>

- 以上图为例。创建特定的软链接
```bash
ln -s <pythonEnvPath>/bin.python ~/bin/python
```
> 一般每个人可能会存在多个python环境，`~/bin/<pythonName>`中的`<pythonName>`其实可以任意命名，便于在使用时区分。
- 使用绝对路径命令进行训练
```bash
~/bin/python train.py
```

- 这样，在杀进程时更容易区分，是哪个程序，可以避免被管理员误杀。如果程序存在异常，易于管理员找到对应的用户。

<div align="center">
    	<img src="./pic/2021-10-05-16-00-07.png"  style="zoom:100%;"">  
</div>

- 使用`ll`指令可以看到每个软链接指向的，原始的命令路径。
```bash
ll ~/bin/
```
<div align="center">
    	<img src="./pic/2021-10-05-16-03-12.png"  style="zoom:100%;"">  
</div>

### 3.2.2 conda迁移python环境

> 通过环境迁移可以将本地或者其他服务器的conda环境打包发送到当前服务器，避免再次部署的麻烦。迁移只能同平台进行，即linux迁移到linux，如果原来的环境在windows，最好的方法还是在服务器新建一个环境。

- 首先安装`conda pack`
```bash
pip install conda-pack
```

- 在`Terminal`，输入下面的指令，例如确定需要迁移的python环境名称
```cmd
conda info --env
```
- 输入以下命令
```bash
conda pack --name <EnvName>
```
- 使用`SFTP`上传文件到服务器的个人文件夹中。然后在终端依次输入下面的指令
```bash
# 在服务器上建立新的python环境文件夹
mkdir -p ~/.conda/envs/<newEnvName>

# 解压旧的python环境文件到新建的文件夹
tar -xvf <oldEnvFile> -C ~/.conda/envs/<newEnvName>
```
> 注意解压时两个路径要根据实际情况修改。

- 可以通过下面的指令，查看环境是否迁移成功
```bash
# 查看env信息
conda info --env

# 激活新的env环境
conda activate <newEnvName>

# 查看新的env环境中的python库
pip list
```


### 3.2.3 Pycharm远程调试

- 直接在本地使用`pycharm`调用服务器的python解释器以及GPU，调试python代码强烈推荐使用这个方式！！！通过配置远程调试实现，csdn百度等有很多教程，具体可以参考下面的链接。

```html
https://blog.csdn.net/autoliuweijie/article/details/80701915
```